# 2. Peintures

## PSY

<figure markdown>

![psy1](../images/art/psy1.jpg){data-gallery="Psy"}
<figcaption>Psy1</figcaption>
</figure>

<figure markdown>

![psy2](../images/art/psy2.jpg){data-gallery="Psy"}

![psy3](../images/art/psy3.jpg){data-gallery="Psy"}

![psy4](../images/art/psy4.jpg){data-gallery="Psy"}

![psy5](../images/art/psy5.jpg){data-gallery="Psy"}

![psy6](../images/art/psy6.jpg){data-gallery="Psy"}

![psy7](../images/art/psy7.jpg){data-gallery="Psy"}

![psy8](../images/art/psy8.jpg){data-gallery="Psy"}

![psy9](../images/art/psy9.jpg){data-gallery="Psy"}

![!](../images/art/psy10.jpg){data-gallery="Psy"}

![!](../images/art/psy11.jpg){data-gallery="Psy"}

![!](../images/art/psy12.jpg){data-gallery="Psy"}

![!](../images/art/psy13.jpg){data-gallery="Psy"}

![!](../images/art/psy14.jpg){data-gallery="Psy"}

![!](../images/art/psy15.jpg){data-gallery="Psy"}

![!](../images/art/psy16.jpg){data-gallery="Psy"}

![](../images/art/psy17.jpg){data-gallery="Psy"}

![](../images/art/psy18.jpg){data-gallery="Psy"}

![](../images/art/psy19.jpg){data-gallery="Psy"}

![](../images/art/psy20.jpg){data-gallery="Psy"}

![](../images/art/psy21.jpg){data-gallery="Psy"}

## FIG

![](../images/art/fig1-min.jpg)

![](../images/art/fig2-min.jpg)

![](../images/art/fig3-min.jpg)

![](../images/art/fig4-min.jpg)

![](../images/art/fig5-min.jpg)

![](../images/art/fig6-min.jpg)

![](../images/art/fig7-min.jpg)

![](../images/art/fig12.jpg)

![](../images/art/fig13.jpg)

## VAR

![](../images/art/var1.jpg)

![](../images/art/var2.jpg)

![](../images/art/var3.jpg)

![](../images/art/var4.jpg)

![](../images/art/var5.jpg)

![](../images/art/var6.jpg)

![](../images/art/var7.jpg)

![](../images/art/var8.jpg)

![](../images/art/var9.jpg)

![](../images/art/var10.jpg)

![](../images/art/var11.jpg)

![](../images/art/var12.jpg)

![](../images/art/var13.jpg)

![](../images/art/var14.jpg)

![](../images/art/var15.jpg)

![](../images/art/var16.jpg)


## ZOOM

![](../images/art/zoom1.jpg)

![](../images/art/zoom2.jpg)

![](../images/art/zoom3.jpg)

![](../images/art/zoom4.jpg)

![](../images/art/zoom5.jpg)

![](../images/art/zoom6.jpg)

![](../images/art/zoom7.jpg)

![](../images/art/zoom8.jpg)

![](../images/art/zoom9.jpg)

![](../images/art/zoom10.jpg)

![](../images/art/zoom11.jpg)

![](../images/art/zoom12.jpg)

![](../images/art/zoom13.jpg)

![](../images/art/zoom14.jpg)

![](../images/art/zoom15.jpg)

![](../images/art/zoom16.jpg)


## MONO

![](../images/art/mono1-min.jpg)

![](../images/art/mono2-min.jpg)

![](../images/art/mono3-min.jpg)

![](../images/art/mono4-min.jpg)

![](../images/art/mono5-min.jpg)

![](../images/art/mono6-min.jpg)

![](../images/art/mono7-min.jpg)

![](../images/art/mono8-min.jpg)

![](../images/art/mono9-min.jpg)


## ARCH

![](../images/art/arch1.jpg)

![](../images/art/arch2.jpg)

![](../images/art/arch3.jpg)

![](../images/art/arch4.jpg)

![](../images/art/arch5.jpg)
