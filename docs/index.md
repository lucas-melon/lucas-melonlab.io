![](images/avatar-photo.jpg)

Bienvenue, sur mon blog personnel.
Ce blog réunit la documentation relative à mes travaux de design, d’architecture et mes œuvres graphiques.


## Mon parcours

Je suis née à Braine-l’Alleud ou j’ai suivi un cursus scientifique avec en parallèle une formation pluridisciplinaire à l’école des arts de Braine-l’Alleud. Je suis ensuite passé à l’IPET de Nivelle pour finir mon cursus avec une option Science industrielle spécialisée dans la construction avant de commencer mes études d’architecture à la faculté La Cambre Horta.

## Mes Travaux

Je tente de diversifier mes créations dans plusieurs domaines artistiques. Outre l’architecture je peins beaucoup (toutes mes peintures sont visibles sur mon compte [Instagram](https://www.instagram.com/lucas_melon_/?hl=fr)) de plus ma formation a l’école des arts ma permit de tester beaucoup de technique (sculpture, gravure, céramique, etc.) que je tente de mêler dans mes projets.

J’ai également développé plusieurs projets de design intégrant des outils numériques comme les découpeuses laser ou les fraiseuses CNC.
