# 4. Construction des niveaux



## Hall de Connecteurs

### Principe

La conception de ce niveau se base sur un hall donnant accès à des pièces périphériques. Les connecteurs permettent de faire la connexion avec les pièces par le centre du hall. Les pièces périphériques sont virtuellement placées à l’intérieur d’un espace réduit donnant l’impression d’un espace plus grand à l’intérieur qu’à l’extérieur. Des ouvertures au plafond permettent au soleil simulé de communiquer des informations sur l’orientation des pièces.

### Schémas

![](../images/MEMO/const1.1.jpg)
Espace conçu

![](../images/MEMO/const1.2.jpg)
Espace perçu

### Unreal Engine

![](../images/MEMO/const1.3.png)

![](../images/MEMO/const1.4.png)

## Connecteurs en Série

### Principe

Le principe de ce niveau est l’utilisation de connecteurs pour optimiser une circulation. Ces connecteurs placés en série créent une ligne droite alternative vers un objectif, une circulation plus courte que la ligne droite. L’axe des connecteurs est perpendiculaire aux pièces, pourtant il permet d’avancer ou de reculer dans l’axe des pièces. Chaque salle prend l’apparence d’une pièce d’exposition.   

### Schémas

![](../images/MEMO/const2.1.jpg)
Espace conçu

![](../images/MEMO/const2.2.jpg)
Espace perçu

### Unreal Engine

![](../images/MEMO/const2.3.png)

![](../images/MEMO/const2.4.png)

## Espace Multiscalaire

### Principe

Ce niveau porte sur notre rapport à l’échelle, les connecteurs de ce niveau ont une particularité, les deux objets composant le connecteur n’ont pas la même dimension. Cette différence me permet de doubler ou de réduire de moitié l’échelle d’un objet ou personnage passant par ce connecteur. Deux paires de connecteurs placés en boucle dans l’espace engendrent une circulation circulaire qui permet de contrôler l’échelle du personnage (un tour complet multiplie son échelle par quatre). L’espace est multiscalaire dans les sens qu’il est explorable à plusieurs échelles différentes.

### Schémas

![](../images/MEMO/const3.1.jpg)
Espace conçu

![](../images/MEMO/const3.2.jpg)
Espace perçu

### Unreal Engine

![](../images/MEMO/const3.3.png)

![](../images/MEMO/const3.4.png)

## Anneaux de Connexion

### Principe

Ce niveau est le premier à utiliser des gravités multiples, l’objectif du niveau est de jouer avec le vide qui compose le décor. Un élément situé sur une face extérieure du cube est seul sur son plan, l’élément est donc visuellement isolé dans l’espace.
Le niveau comporte deux mises en place particulières, deux rampes circulaires permettent de rejoindre toutes les faces du cube. Des connecteurs sont aussi utilisés pour créer des fenêtres simulées sur les autres faces du cube (ces connecteurs sont uniquement visuels, ils ne peuvent pas être utilisés pour se déplacer).

### Schémas

![](../images/MEMO/const4.1.jpg)
Espace conçu

![](../images/MEMO/const4.2.jpg)
Espace perçu

### Unreal Engine

![](../images/MEMO/const4.3.png)

![](../images/MEMO/const4.4.png)

## Espace Compressé

### Principe

Ce niveau est composé de cinq tours, chaque tour représente une pièce et ces pièces sont uniquement accessibles via des connecteurs. L’intérêt de cette mise en place est double, tout d’abord la position des connecteurs détermine le plan, à partir de ces cinq tours une infinité de plans différents sont possibles. Ensuite, chaque pièce étant isolée dans l’espace, elle offre la possibilité de placer des fenêtres sans se soucier des pièces adjacentes.

### Schémas

![](../images/MEMO/const5.1.jpg)
Espace conçu

![](../images/MEMO/const5.2.jpg)
Espace perçu

### Unreal Engine

![](../images/MEMO/const5.3.png)

![](../images/MEMO/const5.4.png)

## Faces Programmatiques

### Principe

Les gravités multiples permettent également d’exploiter toutes les surfaces contrairement à une gravité classique qui ne permet d’utiliser que les surfaces horizontales. La composition débute donc par un cube sur lequel une fonction est attribuée à chaque face, des rampes sont ensuite placées à l’intérieur pour rejoindre toutes les faces.

### Schémas

![](../images/MEMO/const6.1.jpg)
Espace conçu

![](../images/MEMO/const6.2.jpg)
Espace perçu

### Unreal Engine

![](../images/MEMO/const6.3.png)

![](../images/MEMO/const6.4.png)

## Faces Programmatiques B

### Principe

Ce niveau explore le même principe que le niveau précédent, mais les rampes ont été modifiées pour améliorer l’ergonomie et les possibilités d’aménagement. Dans ce niveau seules deux rampes sont présentes, mais chacune de ces rampes connecte quatre faces différentes.

### Schémas

![](../images/MEMO/const7.1.jpg)
Espace conçu

![](../images/MEMO/const7.2.jpg)
Espace perçu

### Unreal Engine

![](../images/MEMO/const7.3.png)

![](../images/MEMO/const7.4.png)

## Ruban Programmatique

### Principe

La conception de ces niveaux est partie de l’idée d’un escalier utilisable dans quatre orientations. En effet, la forme particulière de l’escalier lui permet d’être compatible avec quatre gravités. En connectant les extrémités de l’escalier, j’ai obtenu deux circulations imbriquées qui définissent deux espaces. Chaque circulation traverse quatre faces intérieures et quatre faces extérieures, une circulation occupe un espace d’habitation et l’autre un espace d’exposition.

### Schémas

![](../images/MEMO/const8.1.jpg)
Espace conçu

![](../images/MEMO/const8.2.jpg)
Espace perçu

### Unreal Engine

![](../images/MEMO/const8.3.png)

![](../images/MEMO/const8.4.png)

## Fichiers Finaux

Une fois la construction des niveaux terminée et les règles relatives à la gravité encodées, les niveaux sont préparés pour les sessions de test. Un chronomètre est intégré au programme pour mesurer les temps de parcours et une interface est paramétrée pour indiquer la prochaine balise à atteindre.

Unreal Engine me permet de produire un exécutable qui contient la construction et les règles qui l’accompagnent sans devoir passer par le programme complet. J’ai produit pour ce mémoire deux exécutables, l’un pour Windows qui contient tous les niveaux et un autre pour le dispositif de réalité « virtuelle » (Oculus Quest 2) qui contient les deux niveaux « Faces Programmatiques » et le niveau « Ruban Programmatique ».

Les deux exécutables sont sous forme compressée et doivent être décompressés à l’aide de Winrar avant d’être utilisés.
Le dossier de l’exécutable Windows nommé « MEMO arch imps Windows » doit être placé de préférence dans le dossier « C:\Program Files (x86) », il peut alors être exécuté en tant qu’administrateur grâce au fichier MEMOgravity.exe .

Le dossier de l’exécutable Oculus Quest nommé « UE4Game » doit être placé impérativement dans le dossier « Ce PC\Quest 2\Espace de stockage interne partagé » via un débogage USB du casque. L’application peut alors être lancée depuis l’app Library dans la section « source inconnue ».

- [Executable Windows](https://www.dropbox.com/s/drdw91earea9afv/MEMO%20arch%20imps%20windows.rar?dl=0)

- [Executable Oculus Quest 2](https://www.dropbox.com/s/uiqqjkzb6a4p39o/UE4Game.rar?dl=0)

- [Fiches des resultats](https://www.dropbox.com/s/yst08tul3zkyexu/Architecture%20impossible%20Fiches%20de%20resulats.pdf?dl=0)
