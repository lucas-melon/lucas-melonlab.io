# 8. Source

## Artistes et jeux vidéo

-Piranèse (1720-1778)
HYPERLINK " https://www.universalis.fr/encyclopedie/piranese/ "

-Maurits Cornelis Escher (1898-1972)
HYPERLINK " https://mcescher.com/about/biography/ "

-Gérard Trignac (1955-)
HYPERLINK " http://www.trignac-gerard.com/ "

-Hans Ruedi Giger (1940-2014)
HYPERLINK " https://hrgiger.com/ "

-Tsutomu Nihei (1971-)
HYPERLINK " https://www.glenat.com/auteurs/tsutomu-nihei "

-Tom Ngo (1983-)
HYPERLINK “ https://www.tomngo.net/ ”

-Filip Dujardin (1971-)
HYPERLINK “ https://www.valerietraan.be/filip-dujardin ”

-NaissanceE
HYPERLINK “ https://store.steampowered.com/app/265690/NaissanceE/ ”

-Fragments of Euclid
HYPERLINK “ https://nusan.itch.io/fragments-of-euclid ”

-Portal 2
HYPERLINK “ https://store.steampowered.com/app/620/Portal_2/?l=french ”

-Antichamber
HYPERLINK “ https://store.steampowered.com/app/219890/Antichamber/ ”

-Monument Valley
HYPERLINK " https://www.monumentvalleygame.com/mvpc ”

-Fugue in Void
HYPERLINK " https://store.steampowered.com/app/883220/Fugue_in_Void/ ”

-Manifold Garden
HYPERLINK " https://store.steampowered.com/app/473950/Manifold_Garden/ ”


## Articles de journal et articles de vulgarisation

« Architectural Absurdities by Tom Ngo », 2013. Dezeen [consultable en ligne]. [Consulté le 27 décembre 2021]. Disponible à l’adresse : https://www.dezeen.com/2013/02/17/more-architectural-absurdities-by-tom-ngo/

« Architecture: The Cult Following Of Liminal Space », [sans date]. Musée Magazine [consultable en ligne]. [Consulté le 27 décembre 2021]. Disponible à l’adresse : https://museemagazine.com/features/2020/11/1/the-cult-following-of-liminal-space

« Biography », [sans date]. M.C. Escher - The Official Website [consultable en ligne]. [Consulté le 22 février 2021]. Disponible à l’adresse : https://mcescher.com/about/biography/

Infinite Walking in Virtual Reality | Two Minute Papers #262, [sans date]. [consultable en ligne]. [Consulté le 14 août 2021]. Disponible à l’adresse : https://www.youtube.com/watch?v=KEdrBMZx53w&ab_channel=TwoMinutePapers

« Carrefour a lancé sa première opération de recrutement dans le metaverse », [sans date]. Groupe Carrefour [consultable en ligne]. [Consulté le 3 août 2022]. Disponible à l’adresse : https://www.carrefour.com/fr/actuality/2022/campusmetaverse

« Définition : Qu’est-ce que la réalité augmentée ? », 2017. Artefacto [consultable en ligne]. [Consulté le 3 août 2022]. Disponible à l’adresse : https://www.artefacto-ar.com/realite-augmentee/

« Liminal Space », [sans date]. Aesthetics Wiki [consultable en ligne]. [Consulté le 27 décembre 2021]. Disponible à l’adresse : https://aesthetics.fandom.com/wiki/Liminal_Space

« Strivr raises $30 million to bring VR training to the enterprise », 2020. VentureBeat [consultable en ligne]. [Consulté le 23 juin 2022]. Disponible à l’adresse : https://venturebeat.com/2020/03/31/strivr-raises-30-million-to-bring-vr-training-to-the-enterprise/

« Virtual reality game helps ease pain for burns victims », 2018. BBC News [consultable en ligne]. [Consulté le 23 juin 2022]. Disponible à l’adresse : https://www.bbc.com/news/uk-england-south-yorkshire-43744009

« Virtual reality PTSD treatment has « big impact » for veterans », 2019. BBC News [consultable en ligne]. [Consulté le 14 août 2021]. Disponible à l’adresse : https://www.bbc.com/news/uk-wales-49880915

« What should be in the center of Escher’s painting « Print Gallery »? Is there a single way to finish it? Can we find the original image bef... », [sans date]. Quora [consultable en ligne]. [Consulté le 7 août 2022]. Disponible à l’adresse : https://www.quora.com/What-should-be-in-the-center-of-Eschers-painting-Print-Gallery-Is-there-a-single-way-to-finish-it-Can-we-find-the-original-image-before-distortion

BAYARD, Florian, 2022. Carrefour fait désormais passer des entretiens d’embauche dans le metaverse. PhonAndroid [consultable en ligne]. 19 mai 2022. [Consulté le 3 août 2022]. Disponible à l’adresse : https://www.phonandroid.com/carrefour-fait-desormais-passer-des-entretiens-dembauche-dans-le-metaverse.html

GRINE, Esteban, 2019. Antichambre, Escher & le continuum non-euclidien des jeux vidéo. Chroniques Vidéoludiques [consultable en ligne]. 25 octobre 2019. [Consulté le 22 février 2021]. Disponible à l’adresse : https://www.chroniquesvideoludiques.com/antichambre-escher-le-continuum-non-euclidien-des-jeux-video/

ESCHER, Maurits Cornelis, 2000. M. C. Escher: The Graphic Work. Taschen. ISBN 978-3-8228-5864-6.

ROGUE, Zeno, 2021. Non-Euclidean geometry and games. Medium [consultable en ligne]. 29 janvier 2021. [Consulté le 22 février 2021]. Disponible à l’adresse : https://zenorogue.medium.com/non-euclidean-geometry-and-games-fb46989320d4

VANDERBLIT, Tom, 2015. These Tricks Make Virtual Reality Feel Real. Nautilus | Science Connected [consultable en ligne]. 23 décembre 2015. [Consulté le 23 juin 2022]. Disponible à l’adresse : https://nautil.us/these-tricks-make-virtual-reality-feel-real-4106/

## Articles scientifiques

« Mémoire. Architecture de papier. 2016 by Zaytseva Anna - Issuu », [sans date]. [consultable en ligne]. [Consulté le 11 juillet 2022]. Disponible à l’adresse : https://issuu.com/zaytsevaanna/docs/m__moire_anna_14.04.2016_

« Pentagon Eyeing More Advanced Virtual, Augmented Reality Headwear », [sans date]. [consultable en ligne]. [Consulté le 23 juin 2022]. Disponible à l’adresse : https://www.nationaldefensemagazine.org/articles/2019/11/27/pentagon-eyeing-more-advanced-virtual-augmented-reality-headwear

Pentagon Eyeing More Advanced Virtual, Augmented Reality Headwear
BERGER, Christopher C., GONZALEZ-FRANCO, Mar, OFEK, Eyal et HINCKLEY, Ken, 2018. The uncanny valley of haptics. Science Robotics. 25 avril 2018. Vol. 3, n° 17, pp. eaar7010. DOI 10.1126/scirobotics.aar7010.

BLUMSTEIN, Gideon, 2019. Research: How Virtual Reality Can Help Train Surgeons. Harvard Business Review [consultable en ligne]. 16 octobre 2019. [Consulté le 23 juin 2022]. Disponible à l’adresse : https://hbr.org/2019/10/research-how-virtual-reality-can-help-train-surgeons

DUFILLOT, Olivier, 2014. L’architecture virtuelle. . 2014. pp. 52.

EHRSSON, H. Henrik, 2007. The Experimental Induction of Out-of-Body Experiences. Science. 24 août 2007. Vol. 317, n° 5841, pp. 1048-1048. DOI 10.1126/science.1142175.


FISHER, Joshua, GARG, Amit, SINGH, Karan et WANG, Wesley, 2017. Designing intentional impossible spaces in virtual reality narratives: A case study. Dans : . 1 janvier 2017. pp. 379-380.

FLYNN, Bernadette, 2004. Games as Inhabited Spaces. Media International Australia. 1 février 2004. Vol. 110, n° 1, pp. 52-61. DOI 10.1177/1329878X0411000108.

GÜL KAÇMAZ, Erk et ULUOGLU, Belkis, 2013. CHANGING PARADIGMS IN SPACE THEORIES: Recapturing 20th Century Architectural History. ArchNet-IJAR : International Journal of Architectural Research. Vol. 7. Cambridge, mars 2013. pp. 6-20.
ProQuest Central: 1348212393

HILLARY LEUNG, 2019. AN IMPOSSIBLE SPACE. Time (Chicago, Ill.). 2019. Vol. 194, n° 8/9, pp. 95-.

MATSUMOTO, Keigo, BAN, Yuki, NARUMI, Takuji, YANASE, Yohei, TANIKAWA, Tomohiro et HIROSE, Michitaka, 2016. Unlimited corridor: redirected walking techniques using visuo haptic interaction. Dans : . 24 juillet 2016. pp. 1-2.

PETKOVA, Valeria I. et EHRSSON, H. Henrik, 2008. If I Were You: Perceptual Illusion of Body Swapping. PLOS ONE. 3 décembre 2008. Vol. 3, n° 12, pp. e3832. DOI 10.1371/journal.pone.0003832.

RIEUSSET-LEMARIÉ, Isabelle, 1999. De l’utopie du «musée cybernétique» à l’architecture des parcours dans les musées. Culture & Musées. 1999. Vol. 16, n° 1, pp. 103-128. DOI 10.3406/pumus.1999.1144.

SANDRO VARANO, NICOLAS DESCAMPS, et ERIC TOUVENOT, 2020. Expérimentation d’un EVE pour la co-création spatiale et pédagogique. Dans : . Les Ulis : EDP Sciences. 2020. pp. 04002-.

STEVENSON, Ian, 1995. Constructing curvature: the iterative design of a computer-based microworld for non-Euclidian geometry. [consultable en ligne]. Doctoral. Institute of Education, University of London. [Consulté le 22 février 2021]. Disponible à l’adresse : https://discovery.ucl.ac.uk/id/eprint/10021602/

SUN, Qi, PATNEY, Anjul, WEI, Li-Yi, SHAPIRA, Omer, LU, Jingwan, ASENTE, Paul, ZHU, Suwen, MCGUIRE, Morgan, LUEBKE, David et KAUFMAN, Arie, 2018. Towards virtual reality infinite walking: dynamic saccadic redirection. ACM Transactions on Graphics. 31 août 2018. Vol. 37, n° 4, pp. 1-13. DOI 10.1145/3197517.3201294.

YANG, Feng, 2020. Architecture, Virtual Space And Impossible Space [consultable en ligne]. [Consulté le 22 février 2021]. Disponible à l’adresse : http://urn.kb.se/resolve?urn=urn:nbn:se:kth:diva-281401

YU, Xinge, XIE, Zhaoqian, YU, Yang, LEE, Jungyup, VAZQUEZ-GUARDADO, Abraham, LUAN, Haiwen, RUBAN, Jasper, NING, Xin, AKHTAR, Aadeel, LI, Dengfeng, JI, Bowen, LIU, Yiming, SUN, Rujie, CAO, Jingyue, HUO, Qingze, ZHONG, Yishan, LEE, ChanMi, KIM, SeungYeop, GUTRUF, Philipp, ZHANG, Changxing, XUE, Yeguang, GUO, Qinglei, CHEMPAKASSERIL, Aditya, TIAN, Peilin, LU, Wei, JEONG, JiYoon, YU, YongJoon, CORNMAN, Jesse, TAN, CheeSim, KIM, BongHoon, LEE, KunHyuk, FENG, Xue, HUANG, Yonggang et ROGERS, John A., 2019. Skin-integrated wireless haptic interfaces for virtual and augmented reality. Nature. novembre 2019. Vol. 575, n° 7783, pp. 473-479. DOI 10.1038/s41586-019-1687-0.
