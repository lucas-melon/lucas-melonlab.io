# 3. Méthodologie

Mon but dans ce mémoire est de créer des terrains d’expérimentations numériques afin d’identifier et d’analyser les nouveaux outils d’architecture propres aux espaces impossibles. L’idée est d’explorer les nouvelles possibilités qu’offre un espace impossible en termes de ressenti mais également de programme.
La première étape de ce mémoire est la construction de niveaux numériques, chaque niveau portera sur un principe d’architecture impossible en particulier et sera indépendant des autres. Ces niveaux seront construits numériquement dans un moteur de jeu (Unreal Engine 4.26) afin d’être parcourus à l’aide d’un personnage simulé. Au total, huit niveaux vont être construits explorant sept principes différents.
La seconde phase consiste à évaluer les perceptions architecturales et les effets physiologiques engendrés par ces principes d’architecture impossible. Pour cela, de courtes sessions de test seront effectuées dans le niveau. Ces sessions de test consisteront en la réalisation d’une tâche simple afin d’utiliser l’espace, ce qui me permettra de recueillir des données et d’établir un compte rendu pour chaque niveau.

## Méthode d'experimentation

Pour le contexte d’expérimentation, j’ai fait le choix d’une simulation de travail, de fait cela n’exploite pas l’aspect contemplatif de l’architecture. Le sujet de test va devoir se rendre sur plusieurs « balises » dans un ordre aléatoire dicté par le logiciel. Les balises désignent un cercle de couleur nommé A, B ou C qui marque une position précise dans le niveau. Cette méthode me permet de simuler une fonction dans l’espace et donner un objectif simple au sujet de test, minimiser le temps de parcours entre sa position et celle de la prochaine balise à atteindre, de plus cet objectif encourage une réflexion active sur l’espace pour trouver des raccourcis ou optimiser sa trajectoire. L’enjeu autour des espaces numériques portant sur la possibilité qu’ils deviennent des espaces de travail, introduire une tâche rébarbative dans le processus d’expérimentation me semble pertinent pour évaluer les risques et les bénéfices des principes d’architecture impossible.

![](../images/MEMO/BalA.png)   ![](../images/MEMO/BalB.png)   ![](../images/MEMO/BalC.png)
Exemple de balises dans le niveau Ruban programmatique

Deux sessions de test seront effectuées par niveau, cela me permettra de recueillir des informations sur le temps de parcours entre les différentes balises et de les comparer avec les temps de la seconde session dans le but de pouvoir déterminer si une évolution est notable dans l’appréhension de l’espace et si les impressions ressenties durant la session correspondent au temps réel de parcours.
Un rapport sera rédigé à la fin de chaque session de test contenant une description du parcours effectué dans le niveau et un compte rendu des impressions et effets physiologiques ressentis durant la session.

## Construction des cas d'études

Deux esthétiques seront convoquées au sein des niveaux, celle de l’habitation et celle du musée. Ces deux fonctions simulées conditionneront la présence et l’apparence des « Ancres », le décor de la simulation. La volumétrie et l’organisation d’un niveau seront dictées par le principe d’architecture impossible en présence, cependant tous les niveaux partageront des textures et un mobilier commun afin d’offrir une cohérence globale à l’ensemble des niveaux.
La construction des niveaux est entrecoupée de discussions avec mon promoteur Miguel Pinto Goncalves et Emilio Lopez-Menchero sur les forces et faiblesses d’un principe exploité et sur les enjeux architecturaux et philosophiques du niveau.

## Processus d'expérimentation

Chaque session commence par lancer une minuterie de 20 min et par rejoindre la Balise (A), un nouveau trajet apparaît alors sur le coin supérieur gauche (ex. : AB). Une fois la balise (B) rejointe, le temps passé entre (A) et (B) est enregistré et un nouveau trajet est choisi au hasard entre les 2 possibilités (BA ou BC). Cette procédure est poursuivie jusqu’à la fin de la minuterie de 20 min.
Les ressentis et perceptions architecturales sont notés immédiatement après la session, le parcours est également décrit et dessiné sur un schéma.
La deuxième session est séparée de la première par au moins 2 heures.

Un seul niveau est testé par jour.
Les 5 premiers niveaux sont testés sur ordinateur (les connecteurs ne sont pas compatibles avec la réalité « virtuelle » sur l’Unreal Engine 4.26) et les 3 derniers niveaux (Faces et Ruban programmatique) sont testés sur l’Oculus Quest 2.

Face aux nausées provoquées par le dispositif de réalité « virtuelle » lors de la première session (apparue à 17 minutes du début de la session), le temps des autres sessions en réalité « virtuelle » a été réduit à 15 minutes d’expérimentation.

 


Matériel :

Minuterie de 20 min
Casque Oculus Quest 2 (espace dégagé de 2 m²)
ou
Ordinateur (écran 2560 x 1080 pixels)

Logiciel :

Programme « MEMO-Arch-imps » installé sur l’ordinateur ou directement sur le casque (uniquement compatible avec Oculus Quest 2).


## Communication des résultats

Les données sur le temps de parcours entre les balises se trouvent sous forme de document texte à l’emplacement des dossiers du programme. Ces données seront présentées sous forme de graphique comparatif montrant l’évolution du temps de parcours d’un trajet précis (exemple : de la balise A à la balise C) lors de la première et lors de la seconde session de test.
Un schéma illustrant le parcours effectué lors de la session sera présenté avec la description du parcours. Une représentation axonométrique synthétique sera également produite. La mise en place des éléments de l’axonométrie sera influencée par la structuration du niveau du point de vue de l’utilisateur.
Le compte rendu écrit des perceptions architecturales tentera de définir la capacité d’orientation, la capacité de positionnement géographique des différents éléments du niveau, l’ergonomie et l’appréhension globale du niveau.
