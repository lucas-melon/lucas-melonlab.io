# 2. État de l'art

## Période prénumérique

Durant la période prénumérique, la perspective et la projection constituent les principaux outils de création d’espaces fictionnels. L’architecture utopiste utilise beaucoup de projections et d’axonométries. Les illustrations libres se concentrent au contraire sur la perspective. Notre perception est au cœur de la composition.
La méthode que Piranèse utilise pour générer des espaces impossibles est très différente de celle de M.C. Escher qui se base sur un respect méticuleux des mathématiques. Piranèse préfère jouer avec notre marge de tolérance via de légères modifications des règles de la perspective, suffisamment fortes pour modifier la perception de l’espace, mais pas assez pour être discernables directement.

## Espaces numériques

### Jeux vidéo

La recherche sur l’espace numérique a tout d’abord été développée par les studios de jeux vidéo comme un outil de narration (narration par l’environnement) et un plan pour les mécaniques de jeux. Le principal moyen d’interaction entre le joueur et le développeur est l’espace numérique. Comme pour l’architecture, le rapport entre l’utilisateur et son environnement est au cœur de la réflexion. Les métiers de « Game Designer » et « Level Designer » sont très similaires à celui d’architecte, le premier a un rôle programmatique, il est chargé de définir les objectifs et intentions du jeu, les règles des niveaux et les interactions entre le joueur et l’environnement. Il doit également faire ses choix en fonction de la narration et du principe de jeu (gameplay). Le second métier, « Level Designer » est plus proche d’un rôle de conception architecturale, c’est lui qui positionne et coordonne tous les éléments numériques (objet, personnage, son et lumière) au sein d’un niveau (level) cohérent.

Les espaces impossibles ont donc d’abord été expérimentés dans un but esthétique et symbolique ou comme base d’un système de jeu. Les jeux vidéo cités dans l’introduction ont été capables d’utiliser des principes propres à la simulation pour créer des espaces expérimentables à la portée d’un vaste public. On peut distinguer deux catégories de jeux surreprésentés dans cette sélection, les jeux « puzzle » dont tout le niveau représente un casse-tête géant, la modification de l’espace architectural constitue souvent le principal moyen d’évoluer dans le jeu. L’autre catégorie est le jeu d’exploration qui base son expérience sur la contemplation et la narration par l’environnement. Dans les deux cas, la perception architecturale est au cœur de la réflexion.

### Études réalisées

Cependant, quand ces espaces numériques deviennent des lieux « vécus » non plus exclusivement comme un jeu mais comme un espace social de connexion, la démarche narrative ne suffit plus pour concevoir des espaces qualitatifs pour l’humain. C’est ce que défend Bernadette Flynn dans son article Game as Inhabited Spaces qui s’attarde sur la façon dont on peut se projeter dans un espace numérique pour l’habiter et l’enjeu culturel de la conception de ces espaces. Dans Designing intentional impossible spaces in virtual reality narratives : a case study, Joshua A. Fisher développe son propos sur la création d’un espace de réalité augmentée numérique liée à un campus et les questions politiques sociales et éthiques que cela engendre, mais également des possibilités en termes de connexion et d’échange que ces espaces impossibles superposés à la réalité permettent.

L’aspect pédagogique de l’espace numérique a beaucoup été étudié notamment pour mettre au point des programmes éducatifs instinctifs et ludiques comme le EVE (Educationnal Virtual Environment) par Sandro Varano, Nicolas Descamps et Éric Touvenot qui vise la cocréation spatiale. Cet aspect fut aussi utilisé pour des projets plus utopistes comme la création de musées cybernétiques fonctionnant comme des machines géantes de traitement et diffusion d’information ou comme des espaces de connexion symbolique entre l’humain, l’art et l’humain, cette idée est développée par Isabelle Rieusset-Lemarié dans un article nommé De l'utopie du « musée cybernétique » à l'architecture des parcours dans les musées.

Ce lien avec l’architecture utopique est d’ailleurs énoncé par plusieurs auteurs (Olivier Dufillot dans L’architecture virtuelle, Gül Kaçmaz Erk et Belkis Uluoglu dans CHANGING PARADIGMS IN SPACE THEORIES: Recapturing 20th Century Architectural History) qui placent l’architecture numérique comme descendant direct des architectures dites « de papier » utopistes.
L’architecture numérique est l’aboutissement d’une démarche de création bien plus ancienne dont elle a hérité l’utopisme et la symbolique forte avec cependant une grande différence : pour la première fois, on peut grâce à la virtualité explorer ces espaces théoriques. Le numérique a offert à toutes ces architectures de fiction un endroit où exister.

Ce symbolisme de l’architecture numérique se retrouve souvent dans le programme, à travers des pièces qui représentent seulement symboliquement un espace réel (la présence d’une cuisine n’a pas de sens dans un monde virtuel où l’on ne doit pas manger). Il est donc parfois utilisé pour reconnecter l’humain à un décor abstrait. Cette démarche a été étudiée par Feng Yang dans Architecture, Virtual Space And Impossible Space de façon plus expérimentale lors de son mémoire à travers la création de salles symboliques qui incarnent toute un ressenti architectural différent lié à une pièce particulière, il utilise les espaces impossibles pour amplifier ces ressentis.

## Réalité "Virtuelle"

Beaucoup d’études portent sur la modification de nos perceptions que permet spécifiquement la réalité « virtuelle », notamment dans le domaine de la médecine avec des travaux pour traiter des phobies, pour aider des amputés ou des vétérans.
La plupart de ces utilisations sont possibles grâce à la capacité de la réalité « virtuelle » à simuler un élément dans un cadre contrôlé. On peut de sorte reproduire une phobie ou un traumatisme de façon progressive avec un contrôle à tout instant de l’expérience.  

D’autres projets basés sur la réalité « virtuelle » portent sur la marge entre nos perceptions et nos actions. Keigo Matsumoto et son équipe ont développé dans une expérience nommée Unlimited corridor : redirected walking techniques using visuo haptic interaction un dispositif permettant de faire croire à une marche en ligne droite sur un parcours circulaire de diamètre moyen permettant d’explorer un monde potentiellement infini.

![](../images/MEMO/etat1.jpg)
A gauche la mise en place de l’expérience.           A droite l’espace tel que perçu dans le monde numérique.

<iframe width="560" height="315" src="https://www.youtube.com/embed/THk92rev1VA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Henrik Ehrsson tenta au cours d’une expérience de changer radicalement notre rapport au corps grâce à la réalité « virtuelle » à l’aide d’un dispositif immersif connecté à une caméra située derrière le sujet de test. L’expérience place le sujet dans une position hors de soi, le corps vu dans le casque est difficilement appréhendable comme le nôtre.

On peut donc constater que les impossibilités de l’espace génèrent une perception spatiale différente et par conséquent une perception architecturale nouvelle qui s’accompagne de ses propres outils et ressentis architecturaux. Pourtant, si notre rapport au dispositif numérique a été fortement étudié, peu d’études portent sur notre relation à l’espace numérique par l’intermédiaire d’un dispositif, encore moins sous un angle architectural (la plupart des études visent à réduire l’inconfort/les nausées provoqués par le dispositif).

Mon but dans ce mémoire est donc de tenter de déterminer par l’expérimentation l’impact de ces nouvelles perceptions sur les possibilités architecturales et d’établir les outils, le vocabulaire et les moyens de représentation qui en découlent.


## Lexique

Réalité numérique :

Dans ce mémoire, le terme numérique remplace le terme virtuel souvent utilisé pour décrire tout ce qui est lié à l’informatique. Le terme virtuel désigne une action ou un événement potentiel, une action qui a la possibilité de se produire mais qui n’existe pas encore (par exemple l’énergie potentielle d’un système peut être considérée comme une énergie possédée virtuellement par le système, ou un personnage de fiction peut être considéré comme virtuel). Cependant, nous considérons les interactions et les configurations d’espace informatique comme bien réelles, figées dans un programme qui repose, lui, sur des composants informatiques physiques. Le terme numérique semble donc plus adéquat pour décrire les espaces informatiques, renvoyant uniquement au binaire qui constitue la base de fonctionnement d’une programmation.

Réalité physique :

En opposition avec la réalité numérique dont la base de fonctionnement repose sur le binaire, la réalité physique désigne la réalité avec laquelle nous interagissons avec seulement notre corps dans un contexte géographique particulier et dont la base de fonctionnement repose sur les lois de la physique. Une vision de la réalité qui se résume à notre perception terrestre à l’échelle d’un humain.

Réalité « virtuelle » :

Environnement simulé à l’aide d’un dispositif immersif en temps réel sous forme de casque de réalité « virtuelle » (dispositif de VR). Ce dispositif immersif repose sur la simulation du sens de la vue et du toucher pour créer une illusion jugée crédible par le cerveau. Bien que le mot « Virtuel » soit incorrect comme expliqué plus haut, c’est celui qui s’est imposé lorsque l’on parle de ce type de dispositif.


Niveau :

Un niveau numérique (qui peut aussi être appelé : instance) désigne tout ce qui se situe au sein de la même grille. Un niveau numérique est composé à la base d’un vide infini qui est défini par trois axes X, Y et Z, les éléments numériques sont alors placés sur la grille à des coordonnées précises. Deux niveaux peuvent donc être vus comme des espaces parallèles qui ne permettent pas une continuité d’espace entre les deux.
 

Connecteurs :

Éléments numériques utilisés pour établir une continuité géométrique entre 2 points distants. Ils sont composés d’un cadre qui téléporte tout objet le traversant dans un autre cadre et d’un écran qui est placé dans le cadre et qui affiche dans le premier cadre une vision de la position projetée de l’observateur par rapport au deuxième cadre. Ces éléments numériques sont similaires à ce qui est appelé portail de téléportation dans la science-fiction, cependant le terme portail existant déjà en architecture et même s’il reprend bien l’idée de seuil à franchir, l’idée de connexion entre deux régions géographiques est absente, on préféra alors le terme connecteur.

![](../images/MEMO/lex1.jpg)

Rampe :

Élément numérique utilisé pour établir une continuité entre 2 surfaces d’orientation différente, nécessaire dans les niveaux qui utilisent des gravités multiples. En effet, dans ces niveaux la gravité est définie par un axe perpendiculaire à la surface de contact entre le personnage et le sol. Or pour que la gravité soit calculée par rapport à une nouvelle surface, il faut que celle-ci soit accessible (la différence entre une pente accessible et un mur penché est définie par l’angle par rapport au personnage). La rampe utilise donc des courbes pour échelonner la transition entre 2 surfaces perpendiculaires en de petites étapes de 5° à 10°.

Ancre :

Les ancres sont des éléments numériques archétypaux placés dans les niveaux afin de créer un filet de sécurité contre l’abstraction totale, une trop grande abstraction ne permet plus d’identifier les signaux reçus par le dispositif numérique comme un espace mais comme une image abstraite, ce qui brise l’immersion et l’expérience architecturale. Les ancres (qui prennent ici la forme de mobilier très classique) sont donc placées dans les niveaux pour aider à identifier les espaces et renforcer l’immersion.
 
Caméra :

Dans un espace numérique, l’image affichée sur l’écran ou le dispositif de réalité « virtuelle » est déterminé par la caméra. C’est un objet numérique verrouillé sur la position du personnage qui possède une orientation, une profondeur de champ et un format tout comme une caméra de cinéma. Cette caméra peut être située au niveau des yeux du personnage (on parle alors d’une vue à la première personne) ou pivoter autour de celui-ci (on parle alors de vue à la troisième personne), elle constitue le seul lien visuel avec le niveau, pour un dispositif de réalité « virtuelle » deux caméras sont présentes pour simuler la perception des deux yeux.

UnrealEngine (Moteur de jeux) :

Les moteurs de jeux ont historiquement été créés pour éviter de devoir recommencer à zéro lors de chaque développement d’un nouveau jeu vidéo. Ils se présentent comme des programmes qui contiennent tous les systèmes de base d’un jeu vidéo comme le contrôle d’un personnage lié à une caméra à la première ou troisième personne, une simulation de lumière, et de physique basique ainsi que des options pour intégrer toute fonction supplémentaire. Certains de ces moteurs de jeux sont aujourd’hui accessibles au grand public et forts d’une large communauté qui met à disposition des ressources de travail et des tutoriels pour se former à l’utilisation d’un moteur de jeux.
Le moteur de jeu utilisé pour concevoir et simuler les espaces impossibles de ce mémoire est l’Unreal Engine 4.26 développé par la société Epic Games, société aussi responsable de TwinMotion, un logiciel très utilisé en architecture dans les rendus numériques. TwinMotion est par ailleurs une version simplifiée d’un moteur de jeux dont on n’a gardé que les technologies liées au rendu.
