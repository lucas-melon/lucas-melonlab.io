# 8. Remerciements

Pour finir, j’aimerais remercier Emilio Lopez-Menchero et Miguel Pinto Goncalves pour leur retour régulier durant la conception des niveaux numériques et les discussions passionnantes qui en découlèrent.

J’aimerais également remercier Quentin Boëton (alias Alt236) pour sa vidéo « Les architectes de l’impossible » qui a inspiré l’angle d’analyse de ce mémoire.

Je remercie aussi la communauté Unreal Engine qui m’a permis de me former sur ce programme grâce à leurs nombreux tutoriels disponibles gratuitement sur internet.

Pour finir, je remercie mes parents et mon oncle de leurs relectures et mes colocataires de m’avoir soutenu lors de la rédaction de ce mémoire.
